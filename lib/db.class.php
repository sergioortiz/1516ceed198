<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 27/03/16
 * Time: 15:44
 */

class DB{

    protected $conn;

    /**
     * DB constructor.
     *
     * Variables: $host, $user, $password, $db_name
     *
     */
    public function __construct($host, $user, $password, $db_name,$port = 3306){


/*
        $this->conn=new mysqli($host, $user, $password, $db_name,$port);
        if(mysqli_connect_error()){
            echo "ERROR CONECTANDO".mysqli_connect_error();
            throw new Exception('No se pudo conectar a la BD');
        }else{
            //echo "CONEXION REALIZADA";echo "<br/>";
        }
*/
        try {
            $this->conn=new mysqli($host, $user, $password, $db_name,$port);
        } catch (mysqli_sql_exception $e) {
            throw $e;
        }

    }//Fin constructor

    /**
     *
     * El método lleva a cabo las consultas que se le indiquen.
     *
     * Variable: $sql
     *
     */
    public function query($sql){

        if(!$this->conn){
            return false;
        }

        try{
            $result=$this->conn->query($sql);
        }catch (mysqli_sql_exception $e) {
            throw $e;
        }



        $rowcount=mysqli_num_rows($result);
        if(!$rowcount>0){
            echo "ERROR EN CONSULTA";
            throw new Exception('msqli_error($this->conn)');
        }


        if(is_bool($result)){
            return $result;
        }


        $data=array();

        while ($row=mysqli_fetch_assoc($result)){
            $data[]=$row;
        }
        return $data;


    }//Fin método query

    /**
     *
     * Método para la ejecución de consultas II
     *
     * Variable $sql
     *
     */
    public function inserta($sql){

        //echo "<br/>";echo "SENTENCIA A EJECUTAR: ".$sql;
        if(!$this->conn){
            return false;
        }

        try{
            $result=$this->conn->query($sql);
        }catch (mysqli_sql_exception $e) {
            throw $e;
        }


        if(is_bool($result)){
            return $result;
        }

    }//Fin método inserta


    /**
     *
     * Método para la cuenta de PCs en el sistema.
     *
     * @Variable $sql
     *
     */
    public function cuentaPcs($sql){
        try {
            $result = $this->conn->query($sql);
            $rowcount = mysqli_num_rows($result);
        }catch(mysqli_sql_exception $e) {
            throw $e;
        }
        return $rowcount;
    }//Fin método cuentaPcs.




    /**
     * @param $sql
     */
    public function bloqueados($sql){
        try {
            $valor = $this->conn->query($sql);
        }catch(mysqli_sql_exception $e) {
            throw $e;
        }
        $row = $valor->fetch_array(MYSQLI_NUM);
        return $row[0];

    }//Fin método bloqueados

    /**
     *
     * Método para comprobar si el usuario es admin o no.
     *
     * Variable: $sql
     *
     */
    public function cUser($sql){
        try {
            $valor = $this->conn->query($sql);
        }catch(mysqli_sql_exception $e) {
            throw $e;
        }
        $row = $valor->fetch_array(MYSQLI_NUM);

        if(isset($row[0])){//Se accede si existe registro de resultado positivo.
            //print_r('EXISTE');
            return $row;//Devolvemos el nombre del usuario y si es admin o no.
        }
    }//Fin método cUser




    /**
     * Para prevenir SQL injection
     *
     * Variable $str
     */
    public function escape($str){

        return mysqli_escape_string($this->conn,$str);

    }//Fin método escape


}//Fin clase DB


?>