<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 3/04/16
 * Time: 1:38
 */

class Session{


    /**
     *
     * Método para definir un nuevo valor o modificar alguno del array
     *
     * @variables: $key y $value
     */
    public static function set($key, $value) {
        $_SESSION[$key] = $value;
    }

    /**
     *
     * Método para obtener un valor del array
     *
     * Variable: $key
     *
     */
    public static function get($key) {

        if(isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return null;
    }

    /**
     *
     * M´etodo para destruir un valor del array
     *
     * Variable: $key
     */
    public static function delete($key) {
        if(isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }//Fin método delete

    /**
     *
     * Método para destruir la sesión
     *
     */
    public static function destroy() {
        session_destroy();
    }//Fin método destroy



}//Fin clase

?>