<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 8:06
 */

class Router{
    protected $uri;
    protected $controller;
    protected $action;
    protected $params;
    protected $route;
    protected $methodPrefix;
    protected $language;

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @return mixed
     */
    public function getMethodPrefix()
    {
        return $this->methodPrefix;
    }

    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }



    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @return mixed
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    public function __construct($uri){

        $this->uri = urldecode(trim($uri, '/'));

        $routes = Config::get("routes");
        $this->route = Config::get("default_route");
        $this->methodPrefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
        $this->language = Config::get("default_language");
        $this->controller = Config::get("default_controller");
        $this->action = Config::get("default_action");

        $uriParts = explode('?', $this->uri);
        $path = $uriParts[0];

        $pathParts = explode('/', $path );

        if(count($pathParts)){
            //Obtenemos ruta o idioma
            if(in_array(strtolower(current($pathParts)), array_keys($routes)))
            {
                $this->route = strtolower(current($pathParts));
                $this->methodPrefix = isset($routes[$this->route]) ? $routes[$this->route] : '';
                array_shift($pathParts);
            }elseif(in_array(strtolower(current($pathParts)), Config::get("languages")))
            {
                $this->language = strtolower(current($pathParts));
                array_shift($pathParts);
            }
            //Obtenemos controller = siguiente elemento
            if(current($pathParts)){
                $this->controller = strtolower(current($pathParts));
                array_shift($pathParts);
            }

            //Obtenemos action/método = siguiente elemento
            if(current($pathParts)){
                $this->action= strtolower(current($pathParts));
                array_shift($pathParts);
            }

            //Alamacenamos los posibles parámetros
            $this->params = $pathParts;

        }



    }

}//Fin clase


?>