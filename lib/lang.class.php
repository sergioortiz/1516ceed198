<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 25/03/16
 * Time: 8:48
 */

class Lang{

    protected static $data;//Almacenamos la config. del idioma

    /**
     * Cargar datos del fichero de idiomas
     *
     * Variable $lang_code
     */
    public static function load($langCode){
        $langFilePath=ROOT.DS.'lang'.DS.strtolower($langCode).'.php';

        if(file_exists($langFilePath)){
            self::$data=include($langFilePath);
        }else{
            throw new Exception('Idioma no encontrado, '.$langFilePath);
        }
    }//Fin método load.

    /**
     * Obtenemos el valor del array que se nos indique --> Cadena a imprimir
     *
     * @param $key
     * @param string $defaultValue
     */
    public static function get($key, $defaultValue=''){

        return isset(self::$data[strtolower($key)]) ? self::$data[strtolower($key)] : $defaultValue;

    }//Fin método get.



}//Fin clase

?>