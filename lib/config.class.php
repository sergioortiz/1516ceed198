<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 7:58
 */

class Config
{
    protected static $settings = array();

    /**
     * Método para retornar el valor indicado de $settings
     *
     * Variables: $key
     *
     */
    public static function get($key)
    {
        return isset(self::$settings[$key]) ? self::$settings[$key] : null;
    }//Fin método get

    /**
     * Método para asignar nuevo valor o modificar en $settings
     *
     * Variables: $key y $value
     */
    public static function set($key, $value) {
        self::$settings[$key] = $value;
    }//Fin método set
}

?>