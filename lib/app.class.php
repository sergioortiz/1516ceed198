<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 8:34
 */

class App{

    protected static $router;

    public static $db;
    public static $proxy;

    /**
     * @return mixed
     */
    public static function getRouter()
    {
        return self::$router;
    }


    /**
     *
     * Método para mostrar la vista indicada
     *
     * Variable $uri
     *
     */
    public static function run($uri){

        self::$router=new Router($uri);

        self::$db=new DB(Config::get('db.host'),Config::get('db.user'),Config::get('db.password'),Config::get('db.db_name'));



        //Cargamos el idioma a utilizar
        Lang::load(self::getRouter()->getLanguage());

        $controllerClass = ucfirst(self::$router->getController()) . 'Controller';
        $controllerMethod = strtolower(self::$router->getMethodPrefix() . self::$router->getAction());
        //Llamando al método/action del controlador
        $controllerObject=new $controllerClass();
        if(method_exists($controllerObject,$controllerMethod)){
            //El action del controlador devuelve
            $view_path=$controllerObject->$controllerMethod();
            $viewObject=new View($controllerObject->getData(),$view_path); //Creamos la vista, mandando datos y path.
            $content=$viewObject->render();//Recogemos el contenido
        }else{
            //echo "NO EXISTE";
            throw new Exception ('Método '.$controllerMethod.' de la clase '.$controllerClass.' no existe.');
        }

        $layout=self::$router->getRoute();
        $layoutPath=VIEWS_PATH.DS.$layout.'.html';
        $layoutViewObject=new View(compact('content'),$layoutPath);
        echo $layoutViewObject->render();


    }//Fin método run.


}//Fin clase App




?>