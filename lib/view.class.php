<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 9:27
 */

class View{

    protected $data;//Datos del controlador a la vista.
    protected $path;//Almacenamos la ruta a la vista.


    /**
     *
     */
    public static function getDefaultViewPath(){

        $router=App::getRouter();
        if(!$router){
            return false;
        }
        $controllerDir=$router->getController();
        $plantilla=$router->getMethodPrefix().$router->getAction().'.html';
        return VIEWS_PATH.DS.$controllerDir.DS.$plantilla;

    }//Fin getDefaultViewPath.


    /**
     *
     * View constructor.
     *
     * Variables: array $data, $path
     *
     */
    public function __construct($data=Array(), $path=null){
        //Si no existe path, recuperamos un por defecto.
        if(!$path){
            $path=self::getDefaultViewPath();
        }//Si no existe la plantilla, lanzamos Exception.
        if(!file_exists($path)){
            throw new Exception('Plantilla no encontrada en .'.$path);
        }//Recogemos valores ya que si hemos llegado aquí, hay datos.
        $this->path=$path;
        $this->data=$data;
        //print_r('CONSTRUCTOR_VIEW: '.$this->path.', '.$this->data);echo "<br/>";

    }//Fin construct.

    /**
     *Responsable del rendereing y de devolver el cógido html.
     */
    public function render(){
        $data=$this->data;

        ob_start();
        include($this->path);
        $content=ob_get_clean();

        return $content;

    }//Fin método render

}//Fin clase

?>