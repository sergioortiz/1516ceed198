<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 8:09
 */

require_once(ROOT.DS.'config'.DS.'config.php');

/**
 *
 * Método para autocargar las clases q se soliciten.
 *
 * Variable $className
 *
 */
function __autoload($className) {

    $libPath = ROOT.DS.'lib'.DS.strtolower($className).'.class.php';
    $controllerPath = ROOT.DS.'controllers'.DS.str_replace('controller', '', strtolower($className)).'.controller.php';
    $modelPath = ROOT.DS.'models'.DS.strtolower($className).'.php';

    if(file_exists($libPath)) {
        require_once($libPath);
    } elseif(file_exists($controllerPath)){
        require_once($controllerPath);
    } elseif(file_exists($modelPath)){
        require_once($modelPath);
    }else
    {
        throw new Exception("Fallo al incluir la clase: " . $className);
    }

}

/**
 *
 * Método global para facilitar la obtención de los valores del idioma
 * variable $key, $defaultValue
 *
 */
function __($key, $defaultValue=''){
    return Lang::get($key,$defaultValue);
}


?>