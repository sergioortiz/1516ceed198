<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 8:54
 */

class Controller{

    protected $data;
    protected $model;
    protected $params;


    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @return mixed
     */
    public function getParams()
    {
        return $this->params;
    }

    public function __construct($data=array()){
        $this->data=$data;
        $this->params=App::getRouter()->getParams();

    }//Fin constructor

}//Fin clase Controller

?>