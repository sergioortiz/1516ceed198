<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 8:09
 */

Config::set('nombreSitio','ProxyControl');
Config::set('languages', array('es', 'vl'));

//Rutas. Nombre ruta => prefijo del método
Config::set('routes',
    array(
        'default' => '',
        'admin' => 'admin_' //Ha implementar en la v2 como posible mejora.
    )
);

//Valores por defecto de uri
Config::set('default_route', 'default');
Config::set('default_language', 'es');
Config::set('default_controller', 'pages');
Config::set('default_action', 'index');

//Valores de la BD
Config::set('db.host','localhost');
Config::set('db.user','proxycontrol');
Config::set('db.password','proxycontrol');
Config::set('db.db_name','proxycontrol');


Config::set('aula','1');//Valor a definir en el aula.


?>