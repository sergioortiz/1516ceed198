<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 24/03/16
 * Time: 8:57
 */

class PagesController extends Controller{



    public function __construct($data=array())
    {
        parent::__construct($data);
        $this->model=new Page();//Creamos modelo.



    }


    /**
     *
     * Método encargado del control de la vista
     * correspondiente a la ventana de login.
     *
     * Variables: Las pasadas en $_POST
     *
     */
    public function index(){
        if($_POST){
            $valor=$this->model->cUser($_POST);//Comprobamos existencia de usuario.
            if($valor==false){//No existe
                //print_r('NO EXISTE');
            }else{//Existe
                Session::set('paso','martaynicolas');//Almacenamos variable de control de paso.
                Session::set('usuario',ucfirst($valor[0]));//Almacenamos nombre de usuario.
                Session::set('admin',$valor[1]);//Almacenamos su condición de administrador o no.
                $aula=Config::get('aula');//Recogemos valor definido.
                if(isset($_POST['idioma'])){//Según idioma, cargamos una u otra opción (solo hay una vista).
                    $this->model->usa($valor[2],$aula);//Introducimos datos de uso.
                    header("Location: ./es/pages/tablon");
                }else{
                    $this->model->usa($valor[2],$aula);//Introducimos datos de uso.
                    header("Location: ./vl/pages/tablon");
                }
            }
        }
    }//Fin index.


    /**
     *
     * Método encargado del control de la vista
     * correspondiente a la administración de PCs (altas, bajas, modificaciones).
     *
     * Variables: Las pasadas en $_POST
     *
     */
    public function pcs(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }

        $this->data['info']=$this->model->getPcs();//Invocamos al modelo para obtener la lista de PCs en el aula actualmente.

        $this->data['resultado']=Session::get('resultado');
        Session::set('resultado','');

        if(isset($_POST['crea'])){//Si se seleccionó crear PC.
            $valor=$this->model->nPc($_POST);//Invocamos modelo, la creación de un nuevo PC.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./pcs");
        }elseif(isset($_POST['borra'])) {//Si se seleccionó borrar PC.
            $valor = $this->model->bPc($_POST);//Invocamos modelo, borrar PC.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./pcs");
        }elseif(isset($_POST['actualiza'])){//Si se seleccionó actualizar
            $valor = $this->model->aPc($_POST);//Invocamos modelo, actualizar PC.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./pcs");
        }

    }//Fin método pcs

    /**
     *
     * Método encargado del control de la vista
     * correspondiente a la información de contacto.
     *
     */
    public function detalles(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }
    }//Fin método detalles


    /**
     *
     * Método encargado del control de la vista
     * correspondiente a la administración de Usuarios(altas, bajas, modificaciones).
     *
     * Variables: Las pasadas en $_POST
     *
     */
    public function usuario(){
        $this->data['info']=$this->model->getUsers();//Invocamos modelos para obtener lista de usuarios

        $this->data['resultado']=Session::get('resultado');
        Session::set('resultado','');

        if(isset($_POST['crea'])){//Si se pulsa crear.
            $valor=$this->model->nUser($_POST);//Se invoca al modelo para crear un usuario.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./usuario");
        }elseif(isset($_POST['borra'])) {//Si se pulsa borrar.
            $valor=$this->model->bUser($_POST);//Se invoca al modelo para borrar un usuario.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./usuario");
        }elseif(isset($_POST['actualiza'])){//Si se pulsa actualizar.
            $valor=$this->model->aUser($_POST);//Se invoca al modelo para modificar un usuario.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./usuario");
        }
    }//Fin método usuario


    /**
     *
     * Método encargado del control de la vista
     * correspondiente al tablón principal.
     *
     */
    public function tablon(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }
        $this->data['info'] = $this->model->getMsg();//Invocamos modelo para obtener mensajes del tablón.

    }//Fin método tablon


    /**
     *
     * Método encargado del control de la vista
     * correspondiente a la administración del tablón(altas y bajas).
     *
     * Variables: Las pasadas en $_POST
     *
     */
    public function notastablon(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }

        $this->data['info']=$this->model->getMsg();//Invocamos modelos para obtener mensajes.

        $this->data['resultado']=Session::get('resultado');
        Session::set('resultado','');

        if(isset($_POST['crea'])){//Si se pulsa crear
            $valor=$this->model->nNota($_POST);//Se invoca al modelo para crear una nota.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./notastablon");
        }elseif(isset($_POST['borra'])) {//si se pulsa borrar
            $valor=$this->model->bNota($_POST);//Se invoca al modelo para borrar una nota.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./notastablon");
        }

    }//Fin método notastablon

    /**
     * Método encargado del control de la vista
     * correspondiente a la administración de los PCs(dar Acceso o Bloquear).
     *
     * Variables: Las pasadas en $_POST
     *
     */
    public function cpc(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }
        $this->data['info']=$this->model->getPcs();//Invocamos modelo para obtener lista de PCs.

        if(isset($_POST['actualizar'])){//Si se pulsa actualizar.

            $valor=$this->model->uBd($_POST);//Se invoca modelo para actualizar base de datos.
            $pcs=$this->model->getPcs();//Recogemos info PCs
            $this->model->actua($pcs);//Enviamos a Proxy la info para aplicar reglas.
        }

        if($valor==true){
            header("Location: ./cpc");
        }


    }//Fin método cpc.


    /**
     *
     * Método encargado del control de la vista
     * correspondiente al visionado de la estadísticas de (Accesos vs Bloqueados).
     *
     */
    public function estadistica(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }
            $this->data=$this->model->getDonuts();
    }//Fin método estadística


    /**
     *
     * Método encargado del control de la vista
     * correspondiente al visionado de la gestión del Departamento
     *
     * Variables: Las pasadas en $_POST
     *
     */
    public function dpto(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }
        $this->data['info'] = $this->model->getDpto();//Invocamos modelo para obtener los Departamentos

        $this->data['resultado']=Session::get('resultado');
        Session::set('resultado','');

        if(isset($_POST['crea'])){//Si se pulsa crear
            $valor=$this->model->nDpto($_POST);//Se invoca al modelo para crear una nota.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./dpto");
        }elseif(isset($_POST['borra'])) {//si se pulsa borrar
            $valor=$this->model->bDpto($_POST);//Se invoca al modelo para borrar una nota.
            if($valor=="bien"){Session::set('resultado','Acción realizada correctamente.');}else{Session::set('resultado','Error al ejecutar la acción.');}
            header("Location: ./dpto");
        }
    }//Fin método dpto


    /**
     *
     * Método encargado del control de la vista
     * correspondiente a cerrar sesión.
     *
     * Variables: Las pasadas en $_POST
     */
    public function salir(){
        if(Session::get('paso')!='martaynicolas') {
            header("Location: ./../../");//Volvemos a la vista de login.        }
        }
        if(isset($_POST['salir'])){//Si se pulsa salir.
            Session::set('paso','');//Vaciamos variable de paso.
            header("Location: ./../../");//Volvemos a la vista de login.
        }elseif(isset($_POST['cancelar'])) {//Si se pulsa cancelar.
            header("Location: ./tablon");//Volvemos al tablón.        }
        }
    }//Fin método salir.

}//Fin clase


?>