/*
*
* Método para recoger los datos de la fila de la lista de usuarios
* seleccionada y ubicarlos en el formulario
 */
function myFunction(x) {

    var y = document.getElementById("tabla_usuarios").rows[x.rowIndex];

    document.getElementById("n_codigo").value= y.cells[0].innerText;
    document.getElementById("n_usuario").value= y.cells[1].innerText;
    document.getElementById("n_apell1").value= y.cells[2].innerText;
    document.getElementById("n_apell2").value= y.cells[3].innerText;
    document.getElementById("n_email").value= y.cells[4].innerText;
    document.getElementById("n_dpto").value= y.cells[5].innerText;
    document.getElementById("n_admin").value= y.cells[6].innerText;

    document.getElementById("bCrearUsuario").disabled = true;//Desactivamos el bóton de crear usuario.
}

/*
*
* Método para recoger los datos de la fila de la lista de PCs y ubicarlos
* en el formulario para su actualización.
*
 */

function myFunction2(x) {

    var y = document.getElementById("tabla_pcs").rows[x.rowIndex];

    document.getElementById("pc_codigo").value= y.cells[0].innerText;
    document.getElementById("pc_modelo").value= y.cells[1].innerText;
    document.getElementById("pc_ip").value= y.cells[2].innerText;
    document.getElementById("pc_mac").value= y.cells[3].innerText;
    document.getElementById("pc_nombre").value= y.cells[4].innerText;
    if(y.cells[5].innerText=="Acceso"){
        document.getElementById("pc_internet").value=1;
    }else{
        document.getElementById("pc_internet").value=0;
    }
    //document.getElementById("pc_internet").value= y.cells[5].innerText;
    document.getElementById("pc_aula").value= y.cells[6].innerText;

    document.getElementById("bCrearPc").disabled = true;//Desactivamos el bóton de crear nota.
}

/*
*
* Método para recoger los datos de la fila del tablón de notas
* y ubicarlos en el formulario para su actualización.
*
*/

function myFunction3(x) {

    var y = document.getElementById("tabla_notas").rows[x.rowIndex];

    document.getElementById("nt_codigo").value= y.cells[0].innerText;
    document.getElementById("nt_aula").value= y.cells[1].innerText;
    document.getElementById("nt_msg").value= y.cells[2].innerText;
    document.getElementById("nt_fec").value= y.cells[3].innerText;

    document.getElementById("bCrearNota").disabled = true;//Desactivamos el bóton de crear nota.

}

/*
*
* Método para recoger los datos de la fila de los
* departamentos y ubicarlos en el formulario para
* su actualización.
*/
function myFunction4(x) {

    var y = document.getElementById("tabla_dptos").rows[x.rowIndex];

    document.getElementById("dpt_codigo").value= y.cells[0].innerText;
    document.getElementById("dpt_nombre").value= y.cells[1].innerText;

    document.getElementById("dptoCrear").disabled = true;//Desactivamos el bóton de crear departamento.

}


