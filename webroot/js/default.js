$(document).ready(function(){
	$('[data-toggle="offcanvas"]').click(function(){
		$('#side-menu').toggleClass('hidden-xs');
	});

	$('#refresco').click(function() {
		location.reload();

	});

	$('#refresco2').click(function() {
		location.reload();

	});
	$('#refresco3').click(function() {
		location.reload();

	});



	/*
	 *
	 * Método para limpiar el formulario del tablón y activar el bóton de crear.
	 *
	 */

	if ( document.getElementById( "limpia" )) {
		$('#limpia').click(function () {

			document.getElementById("bCrearNota").disabled = false;//Activamos el bóton de crear nota.

			document.getElementById("nt_codigo").value= "";
			document.getElementById("nt_aula").value= "";
			document.getElementById("nt_msg").value= "";
			document.getElementById("nt_fec").value= "";

		});
	}


	/*
	 *
	 * Método para limpiar el formulario los usuarios y activar el bóton de crear.
	 *
	 */
	if ( document.getElementById( "limpiaUsuario" )) {
		$('#limpiaUsuario').click(function () {

			document.getElementById("bCrearUsuario").disabled = false;//Activamos el bóton de crear nota.

			document.getElementById("n_codigo").value= "";
			document.getElementById("n_usuario").value= "";
			document.getElementById("n_apell1").value= "";
			document.getElementById("n_apell2").value="";
			document.getElementById("n_dpto").value= "";
			document.getElementById("n_admin").value= "";

		});
	}

	/*
	 *
	 * Método para limpiar el formulario de los PCs y activar el bóton de crear.
	 *
	 */
	if ( document.getElementById( "limpiaPc" )) {
		$('#limpiaPc').click(function () {

			document.getElementById("bCrearPc").disabled = false;//Activamos el bóton de crear nota.

			document.getElementById("pc_codigo").value= "";
			document.getElementById("pc_modelo").value= "";
			document.getElementById("pc_ip").value= "";
			document.getElementById("pc_mac").value= "";
			document.getElementById("pc_nombre").value= "";
			document.getElementById("pc_internet").value= "";
			document.getElementById("pc_aula").value= "";

		});
	}


	/*
	 *
	 * Método para limpiar el formulario de los Departamentos y activar el bóton de crear.
	 *
	 */
	if ( document.getElementById( "Dptolimpia" )) {
		$('#Dptolimpia').click(function () {

			document.getElementById("dptoCrear").disabled = false;//Activamos el bóton de crear departamento.

			document.getElementById("dpt_codigo").value= "";
			document.getElementById("dpt_nombre").value= "";


		});
	}


	//Iniciación y ajuste de los botones toggle.

	$('.tog').bootstrapToggle();
	$(function() {
		$('.tog').change(function() {

			$nom=$(this).attr('name');

			$nom="i"+$nom;


			if( $(this).is(':checked') ){

				$("#"+ $nom).val('1');

			} else {

				$("#"+ $nom).val('0');

			}

		})
	});

	//Definición de los pasos del tour

	var tour = new Tour({
		storage:false,
		steps: [
			{
				element: ".t1",
				title: "Tablón",
				backdrop: true,
				content: "Desde aquí los usuarios pueden leer mensajes que el administrador deje."
			},
			{
				element: ".t2",
				title: "Control PC",
				backdrop: true,
				content: "Desde aquí se podrá bloquear el acceso de los ordenadores a internet."
			},
			{
				element: ".t3",
				title: "Detalles",
				backdrop: true,
				content: "Aquí se puede encontrar información de contacto conmigo."
			},
			{
				element: ".t4",
				title: "Estadística",
				backdrop: true,
				content: "Desde aquí se muestra una simple estadística de conectados/bloqueados."
			},
			{
				element: ".t5",
				title: "Logout",
				backdrop: true,
				content: "Haremos uso de esta opción para salir.",
				//path: "./cpc"
			}
		]
	});
	//tour.init(true);//Cargar
	//tour.start(true);//Inicializar

	//Evento que inicia el tour(ayuda)

	$('.tourr').click(function(){
		//alert("hola");
		tour.init(true);//Cargar
		tour.start(true);
	});


	//Gráfico donut

	if ( document.getElementById( "accesos" )) {
		var opcsi=document.getElementById("accesos").value;
		var opcno=document.getElementById("bloqueados").value;
		opcsi=parseInt(opcsi);
		opcno=parseInt(opcno);
		var total=opcsi+opcno;
		//alert("SIs: "+opcsi+", y NOs: "+opcno+" TOTAL: "+total);

		var bigDoughnutData = [
			{value:opcno,color:"#ff4d4d"},/*32*/
			{value:total-opcno,color:"#dce0df"}/*100-32*/
		];

		$( "#myBigDoughnut" ).doughnutit({
			dnData: bigDoughnutData,
			dnSize: 200,/*500*/
			dnInnerCutout: 60,
			dnAnimation: true,
			dnAnimationSteps: 60,
			dnAnimationEasing: 'linear',
			dnStroke: false,
			dnShowText: true,
			dnFontSize: '12px',
			dnFontOffset: 60,
			dnFontColor: "#819596",
			dnText: '',
			dnStartAngle: -90,
			dnCounterClockwise: false,


		});// End Doughnut
	}


	//Login

	$('.idioma').bootstrapToggle();
	$(function() {
		$('.idioma').change(function() {

			if( $(this).is(':checked') ){

				//alert("ESTA ES");
				var idioma="es";
				//setTimeout("window.location.href=\"./es\";", 1000);

			} else {

				//alert("ESTA VL");
				var idioma="vl";
				//setTimeout("window.location.href=\"./vl\";", 1000);
			}

		})
	});





});

