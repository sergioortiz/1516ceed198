<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 25/03/16
 * Time: 8:45
 */

return array(
    'lng.test' => "Ejemplo",
    'lng.login' => 'Introduzca datos',
    'lng.usuario' => 'Usuario',
    'lng.contrasena' => 'Password',
    'lng.recuerdame' => 'Recuerdame',
    'lng.acceder' => 'Acceder',
    'lng.mtablon' => 'Tablón',
    'lng.mcpc' => 'Control PC',
    'lng.madmin' => 'Administración',
    'lng.musuarios' => 'Usuarios',
    'lng.mpcs' => 'PCs',
    'lng.mdetalles' => 'Detalles',
    'lng.mestadis' => 'Estadística',
    'lng.mtour' => 'Tour',
    'lng.mout' => 'Cerrar sesión',
    'lng.mbienvenido' => 'Bienvenido ',
    'lng.mmsg' => 'Mensaje',
    'lng.resultado' => 'RESULTADO',
    'lng.tcodigo' =>'Código',
    'lng.tnombre' => 'Nombre',
    'lng.tapell1' => 'Apellido1',
    'lng.tapell2' => 'Apellido2',
    'lng.temail' => 'Email',
    'lng.tdpto' => 'Departamento',
    'lng.tadmin' => 'Admin',
    'lng.tadmin2' => 'Administrador',
    'lng.bactualizar' => 'Actualizar',
    'lng.bborrar' => 'Borrar',
    'lng.blimpiar' => 'Limpiar',
    'lng.bcrear' => 'Crear',
    'lng.tgusuarios' => 'Gestión de usuarios',
    'lng.tgordenadores' => 'Gestión de ordenadores',
    'lng.tmodelo' => 'Modelo',
    'lng.tip' => 'IP',
    'lng.tmac' => 'MAC',
    'lng.tinternet' => 'Internet',
    'lng.taula' => 'Aula',
    'lng.mordenadores' => 'Ordenadores',
    'lng.notastablon' => 'Gestión de notas',
    'lng.ntmsg' => 'Mensajes',
    'lng.ntfec' => 'Fecha',
    'lng.ntaula' => 'Aula',
    'lng.navegacion' => 'Navegación',
    'lng.tcpc' => 'Control de PCs',
    'lng.tcpclistado' => 'Listado de PCs',
    'lng.mtour' => 'Ayuda',
    'lng.acceso' => 'Acceso',
    'lng.bloqueado' => 'Bloqueado',
    'lng.salir' => '¿Salir?',
    'lng.salir2' => 'Salir',
    'lng.mdpto' => 'Dpto',
    'lng.gdpto' => 'Gestión de Departamento',
    'lng.gnotas' => 'Gestión de notas',
    'lng.dptnombre' => 'Departamento',
    'lng.cancelar' => 'Cancelar',
    'lng.msgdetalles' => 'Persona de contacto: Sergio Ortiz Agudo <br/>
                            email: sortiz@iesgadea.es<br/>
                            Departamento de Informática <br/>
                            IES Salvador Gadea <br/>
                            Camino de las encrucijadas 4 <br/>
                            Aldaia (Valencia)'
);

?>