<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 25/03/16
 * Time: 8:46
 */
return array(
    'lng.test' => 'Exemple',
    'lng.login' => 'Introdueix dades',
    'lng.usuario' => 'Usuari',
    'lng.contrasena' => 'Contrasenya',
    'lng.recuerdame' => 'Recorda\'m',
    'lng.acceder' => 'Accedir',
    'lng.mtablon' => 'Taulell',
    'lng.mcpc' => 'Control PC',
    'lng.madmin' => 'Administració',
    'lng.musuarios' => 'Usuaris',
    'lng.mpcs' => 'PCs',
    'lng.mdetalles' => 'Detalls',
    'lng.mestadis' => 'Estadística',
    'lng.mtour' => 'Tour',
    'lng.mout' => 'Tancar sessió',
    'lng.mbienvenido' => 'Benvingut ',
    'lng.mmsg' => 'Missatge',
    'lng.resultado' => 'RESULTAT',
    'lng.tcodigo' =>'Codi',
    'lng.tnombre' => 'Nom',
    'lng.tapell1' => 'Cognom1',
    'lng.tapell2' => 'Cognom2',
    'lng.temail' => 'Email',
    'lng.tdpto' => 'Departament',
    'lng.tadmin' => 'Admin',
    'lng.tadmin2' => 'Administrador',
    'lng.bactualizar' => 'Actualitzar',
    'lng.bborrar' => 'Esborrar',
    'lng.blimpiar' => 'Neteja',
    'lng.bcrear' => 'Crear',
    'lng.tgusuarios' => 'Gestió d\'usuaris',
    'lng.tgordenadores' => 'Gestió d\'ordinadors',
    'lng.tmodelo' => 'Model',
    'lng.tip' => 'IP',
    'lng.tmac' => 'MAC',
    'lng.tinternet' => 'Internet',
    'lng.taula' => 'Aula',
    'lng.mordenadores' => 'Ordinadors',
    'lng.notastablon' => 'Gestió de notes',
    'lng.ntmsg' => 'Missatges',
    'lng.ntfec' => 'Data',
    'lng.ntaula' => 'Aula',
    'lng.navegacion' => 'Navegació',
    'lng.tcpc' => 'Control de PCs',
    'lng.tcpclistado' => 'Llistat de PCs',
    'lng.acceso' => 'Accés',
    'lng.bloqueado' => 'Bloquejat',
    'lng.salir' => 'Eixir?',
    'lng.salir2' => 'Eixir',
    'lng.mdpto' => 'Dpt',
    'lng.dptnombre' => 'Departament',
    'lng.gdpto' => 'Gestió de Departament',
    'lng.gnotas' => 'Gestió de notes',
    'lng.cancelar' => 'Cancel·lar',
    'lng.msgdetalles' => 'Persona de contacte: Sergio Ortiz Agudo <br/>
                            email: sortiz@iesgadea.es<br/>
                            Departament d\'Informàtica <br/>
                            IES Salvador Gadea <br/>
                            Camí de las encrucijadas 4 <br/>
                            Aldaia (Valencia)'
);

?>
