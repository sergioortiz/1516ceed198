<?php
/**
 * Created by PhpStorm.
 * User: Sergio Ortiz Agudo
 * Date: 27/03/16
 * Time: 18:40
 */
class Page extends Model{

    /**
     *
     * Método que monta la sql para devolver
     * los mensajes del tablón.
     *
    */
    public function getMsg(){

        $sql="select * from tablon ORDER BY idtablon DESC";

        return $this->db->query($sql);
    }//Fin método getMsg


    /**
     *
     * Método que monta la sql para los usuarios
     * que existen en el sistema.
     *
     *
     */
    public function getUsers(){
        $sql="select * from usuarios ORDER BY codigousu ASC";

        return $this->db->query($sql);
    }//Fin método getUsers

    /**
     *
     * Método que devuelve los PCs que hay
     * en el sistema dados de alta.
     *
     *
     */
    public function getPcs(){
        $sql="select * from equipo ORDER BY nombre ASC";

        return $this->db->query($sql);
    }//Fin método getPcs.



    /**
     *
     * Insertar un nuevo usuario
     *
     * Variable $data
     *
     */
    public function nUser($data){

        if($data['n_password']==""){
            $data['n_password']=$data['n_usuario'];
        }
        $data['n_password']=md5($data['n_password']);
        $nombre=$this->db->escape($data['n_usuario']);
        $apell1=$this->db->escape($data['n_apell1']);
        $apell2=$this->db->escape($data['n_apell2']);
        $email=$this->db->escape($data['n_email']);
        $admin=$this->db->escape($data['n_admin']);
        $dpto=$this->db->escape($data['n_dpto']);
        $password=$this->db->escape($data['n_password']);



        $sql="INSERT INTO usuarios(  `codigousu` ,  `nombre` ,  `apell1` ,  `apell2` ,  `email` ,  `password` ,  `departamento` ,  `admin` )
VALUES (
NULL ,  '{$nombre}' ,  '{$apell1}',  '{$apell2}',  '{$email}' ,  '{$password}',  '{$dpto}',  '{$admin}'
)";


        $valor=$this->db->inserta($sql);

        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }

    }//Fin método nUser


    /**
     *
     * Método para borrar un usuario.
     *
     * Variable $_POST
     *
     */
    public function bUser($data){
        $email=$this->db->escape($data['n_email']);

        //print_r('EL EMAIL A BORRAR: '.$email);
        $sql="delete from proxycontrol.usuarios WHERE `email`='$email'";
        //print_r($sql);

        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }
    }//Fin método bUser


    /**
     *
     * Método para actualizar datos de un usuario
     *
     * Variable $data
     *
     */
    public function aUser($data){

        $codigousu=$this->db->escape($data['n_codigo']);
        $nombre=$this->db->escape($data['n_usuario']);
        $apell1=$this->db->escape($data['n_apell1']);
        $apell2=$this->db->escape($data['n_apell2']);
        $email=$this->db->escape($data['n_email']);
        $admin=$this->db->escape($data['n_admin']);
        $dpto=$this->db->escape($data['n_dpto']);
        if($data['n_password']==""){ //Si no se introdujo password, no se cambia.

            $sql="update usuarios
                    set `nombre`= '$nombre',
                    `apell1`='$apell1',
                    `apell2`='$apell2',
                    `email`='$email',
                    `departamento`='$dpto',
                    `admin`='$admin'
                    WHERE `codigousu` = '$codigousu'
            ";
        }else{//Se introdujo password, se actualizará también.
            $password=md5($data['n_password']);
            $password=$this->db->escape($password);
            $sql="update usuarios
                    set `nombre`= '$nombre',
                    `apell1`='$apell1',
                    `apell2`='$apell2',
                    `email`='$email',
                    `password`='$password',
                    `departamento`='$dpto',
                    `admin`='$admin'
                    WHERE `codigousu` = '$codigousu'
            ";
        }

        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }


    }//Fin método aUser.

    /**
     *
     * Método para añadir un PC al sistema
     *
     * Variable $data
     *
     */
    public function nPc($data){
        $model=$this->db->escape($data['pc_modelo']);
        $ip=$this->db->escape($data['pc_ip']);
        $mac=$this->db->escape($data['pc_mac']);
        $nombre=$this->db->escape($data['pc_nombre']);
        $internet=$this->db->escape($data['pc_internet']);
        $dpto=$this->db->escape($data['pc_aula']);



        $sql="INSERT INTO equipo(  `codigoe` ,  `modelo` ,  `ip` ,  `mac` ,  `nombre` ,  `internet` ,  `codaula`)
VALUES (
NULL ,  '{$model}' ,  '{$ip}',  '{$mac}',  '{$nombre}' ,  '{$internet}',  '{$dpto}'
)";




        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }


    }//Fin nPC.

    /**
     *
     * Método para borrar PCs
     *
     * Variables $data
     *
     */
    public function bPc($data){
        $codigo=$this->db->escape($data['pc_codigo']);
        $sql="delete from proxycontrol.equipo WHERE `codigoe`='$codigo'";


        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }
    }//Fin método bPc

    /**
     *
     * Método para actualizar datos de un PC
     *
     * Variable: $data
     *
     */
    public function aPc($data){

        $codigoe=$this->db->escape($data['pc_codigo']);
        $modelo=$this->db->escape($data['pc_modelo']);
        $ip=$this->db->escape($data['pc_ip']);
        $mac=$this->db->escape($data['pc_mac']);
        $nombre=$this->db->escape($data['pc_nombre']);
        $internet=$this->db->escape($data['pc_internet']);
        $aula=$this->db->escape($data['pc_aula']);



            $sql="update equipo
                    set `modelo`= '$modelo',
                    `ip`='$ip',
                    `mac`='$mac',
                    `nombre`='$nombre',
                    `internet`='$internet',
                    `codaula`='$aula'
                    WHERE `codigoe` = '$codigoe'
            ";

        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }
    }//Fin método aPc.


    /**
     *
     * Método para crear una nota
     *
     * Variable: $data
     *
     */
    public function nNota($data){
        $aula=$data['nt_aula'];
        $mensaje=$data['nt_msg'];
        $fecha=$data['nt_fec'];


        $sql="INSERT INTO tablon(  `idtablon` ,  `aula` ,  `mensaje` ,  `fecha`)
VALUES (
NULL ,  '{$aula}' ,  '{$mensaje}',  '{$fecha}'
)";




        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }
    }//Fin método nNota

    /**
     *
     * Método para borrar un nota
     *
     * Variable: $data
     *
     */
    public function bNota($data){
        $codigo=$data['nt_codigo'];
        $sql="delete from tablon WHERE `idtablon`='$codigo'";

        $valor=$this->db->inserta($sql);
        print_r('VALOR: '.$valor);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }
    }//Fin método bNota.


    /**
     *
     * Método para actualizar el estado de acceso o bloqueado
     *
     * Variable: $data
     *
     */
    public function uBd($data){
        $sql="select * from equipo";
        $num=$this->db->cuentaPcs($sql);

        $cont=1;
        for($cont;$cont<=$num;$cont++){

            $internet=$data['int'.$cont];

            if($cont<10){
                $nombre="0".$cont;
                $sql="update equipo
                    set `internet`= '$internet'
                    WHERE `nombre` = '$nombre'
                ";
            }else{
                $nombre=$cont;
                $sql="update equipo
                    set `internet`= '$internet'
                    WHERE `nombre` = '$nombre'
                ";
            }
            $this->db->inserta($sql);

        }
        return true;
    }//Fin método uBd

    /**
     *
     * Método para recolectar datos para la estadística
     *
     */
    public function getDonuts(){
        $sql="SELECT COUNT(internet) AS internet FROM equipo WHERE internet=1 GROUP BY internet";

        $data['si']=$this->db->bloqueados($sql);
        $sql="SELECT COUNT(internet) AS internet FROM equipo WHERE internet=0 GROUP BY internet";
        $data['no']=$this->db->bloqueados($sql);
        //print_r('SI: '.$data['si'].', NO: '.$data['no']);
        return $data;
    }//Fin método getDonuts


    /**
     *
     * Método para comprobar existencia de usuario
     *
     * Variable: $data
     *
     */
    public function cUser($data){
        $email=$data['inputEmail'];
        $pass=md5($data['inputPassword']);
        $sql="SELECT nombre,admin,codigousu from usuarios where `email`='{$email}' and `password`='{$pass}'";
        $valor=$this->db->cUser($sql);

        if(!is_null($valor)){
            //print_r('EXISTE');
            return $valor;
            //return true;

        }else{
            //print_r('NO EXISTE');
            return false;
        }


    }

    /**
     * Almacenamiento de datos de la tabla usa.
     *
     * Variable: $codigousu, $aula
     *
     */
    public function usa($codigousu,$aula){
        //$fecha=date("d/m/Y");
        $fecha=date("Y/m/d");
        $time = time();
        $hora=date("H:i:s", $time);

        //print_r('CodigoUsuario: '.$codigousu.' AULA: '.$aula.' FECHA: '.$fecha.' HORA: '.$hora);
        $sql="INSERT INTO usa(  `codaula` ,  `codusu` ,  `fecha` ,  `hora` )
VALUES (
'{$aula}' ,  '{$codigousu}',  '{$fecha}',  '{$hora}' )";

        $valor=$this->db->inserta($sql);

    }//Fin método usa.


    /**
     *
     * Método que devuelve los departamentos
     * que hay dados de alta en el sistema.
     *
     */
    public function getDpto(){
        $sql="select * from departamento ORDER BY codigodpt ASC";

        return $this->db->query($sql);

    }//Fin método getDpto

    /**
     *
     * Método para añadir un departamento al sistema.
     *
     * @param $data
     *
     */
    public function nDpto($data){

        $nombre=$data['dpt_nombre'];
        $nombre=$this->db->escape($nombre);

        $sql="INSERT INTO departamento(  `codigodpt` ,  `nombre` )
VALUES (
NULL ,  '{$nombre}'
)";




        $valor=$this->db->inserta($sql);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }

    }//Fin método nDpto

    /**
     *
     * Método para borrar un departamento del sistema.
     *
     * Variable: $data
     *
     */
    public function bDpto($data){
        $codigo=$data['dpt_codigo'];
        $sql="delete from departamento WHERE `codigodpt`='$codigo'";

        $valor=$this->db->inserta($sql);
        //print_r('VALOR: '.$valor);
        if($valor==1){
            return "bien";
        }else{
            return "mal";
        }
    }//Fin método bDpto

    /**
     *
     * Método para aplicar nuevas reglas al proxy
     *
     * Variable: $data
     *
     */
    public function actua($data){
        //Reseteamos la configuración del proxy y añadimos las reglas para NAT
        $salida=exec('sudo iptables -F');//Limpiamos reglas
        $salida=exec('sudo iptables -t nat -F');//Limpiamos reglas NAT
        $salida=exec('sudo iptables -P INPUT ACCEPT');//Se acepta todo por defecto a la entrada
        $salida=exec('sudo iptables -P OUTPUT ACCEPT');//Se acepta todo por defecto a la salida
        $salida=exec('sudo iptables -P FORWARD ACCEPT');//Se acepta todo en Forward
        $salida=exec('sudo iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE');//eth0 NIC conectada a Internet.NAT
        $salida=exec('sudo iptables -A FORWARD -i eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT');//NAT

        /* Modificaremos solamente aquellos a los que se les deba cortar el acceso a internet ya que por defecto al
        *  inicio todos tienen acceso.
        */

        foreach($data as $pc){
            if($pc['internet']==0){
                $ip=$pc['ip'];
                $salida=exec('sudo iptables -A FORWARD -s '.$ip.' -j DROP');
            }
        }

        //Almacenamos reglas para cuando se reinicie el servidor proxy se mantenga la misma configuración.

        $salida=exec('sudo iptables-save >/etc/iptables');

    }//Fin método actua.

}//Fin clase Page


?>